This is a project of Avy verifier and extra dependencies

To install/build
===============
BRANCH_NAME is one of dev, cav14, or may14
 
  > git checkout -b LOCAL_NAME BRANCH_NAME

  > git submodule init

  > git submodule update

  > mkdir build ; cd build

  > cmake ../

  > make

The main executable is `build/avy/src/avy'

[Arie Gurfinkel](http://arieg.bitbucket.org) and [Yakir Vizel](http://www.cs.technion.ac.il/~yvizel/)